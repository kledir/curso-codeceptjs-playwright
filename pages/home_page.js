const { I } = inject();

module.exports = {
  fields:{

  },

  button:{
    cadastro: '.fa-lock'
  },

  acessarTelaCadastro(){
    I.click(this.button.cadastro);
    I.waitForText('Cadastro de usuário',10);
  }
}
