const { I } = inject();

module.exports = {
  fields:{
    nomeUsuarioFields: '#user',
    emailUsuarioFields: '#email',
    senhaUsuarioFields: '#password'
  },

  button:{
    cadastrarUsuarioButton: '#btnRegister'
  },

  preencherCamposNovoUsuario(nome, email, senha){   
    I.fillField(this.fields.nomeUsuarioFields, nome);
    I.fillField(this.fields.emailUsuarioFields, email);
    I.fillField(this.fields.senhaUsuarioFields, senha); 
  },
  
  cadastrarNovoUsuario(){
    I.click('#btnRegister')
    I.see('Cadastro realizado!')
  }
}
 