const { generate, validate } = require('gerador-validador-cpf')

const criaCPF = () => {
    var cpf = generate({format: true})
    var cpfAndName = cpf + ' - Kledir Dalcóquio'

    return cpfAndName
}

exports.criaCPF = criaCPF //Esse comando torna essa função Pública para o todo o projeto.