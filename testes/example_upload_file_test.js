Feature ('Example Upload File')

Scenario ('Fazendo upload de imagem', ({ I }) => {
    I.amOnPage('https://automationexercise.com/contact_us')

    I.fillField('name', 'Kedo Bnu')
    I.fillField('email', 'kedo@teste.com.br')
    I.fillField('subject', 'Teste de envio de imagem')
    I.fillField('#message', 'Este é um teste de de envio de imagem pelo codeceptJS')

    pause()
    I.attachFile('.form-control', 'images/image_test.png')
    
    I.click('submit')        
}).tag('upload');