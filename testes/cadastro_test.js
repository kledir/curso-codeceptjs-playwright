//var validacao = require('assert');
const { faker } = require('@faker-js/faker');
const { I, home_page, cadastro_page, login } = inject();

Feature('cadastro');

Scenario ('Login com sucesso', async () => { 
    await login('user') 
})

Scenario('Teste de Cadastro', () => { 

    I.amOnPage('http://automationpratice.com.br/');

    //página home 
    home_page.acessarTelaCadastro()

    //página de cadastro
    var nome = faker.name.fullName()
    var email = faker.internet.email()
    var senha = secret('654321')

    cadastro_page.preencherCamposNovoUsuario(nome, email, senha)
    cadastro_page.cadastrarNovoUsuario()
   
}).tag('cadastro')