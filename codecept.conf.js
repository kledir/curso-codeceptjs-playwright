const {
  setHeadlessWhen,
  setCommonPlugins
} = require('@codeceptjs/configure');
const { firefox } = require('playwright');
// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

// enable all common plugins https://github.com/codeceptjs/configure#setcommonplugins
setCommonPlugins();

const server = require('./server/server.js') //importando o arquivo server.js para chamada do bootstrap e teardown

/** @type {CodeceptJS.MainConfig} */
exports.config = {
  tests: './testes/*_test.js',
  output: './output',
  helpers: {
    Playwright: {
      url: 'http://localhost',
      browser: process.env.NAVEG, //'firefox' //chromium
      show: true //mudando pra FALSE o teste entra em modo Headless
    }
  },
  plugins:{
    autoLogin: {
      enabled: true,
      saveToFile: true,
      inject: 'login',
      users: {
        user: {
          // loginAdmin function is defined in `steps_file.js`
          login: (I) => {
            I.amOnPage('https://automationexercise.com/')
            I.click('.fa-lock')
            I.waitForText('Login to your account',10)
            I.fillField('email', 'kledir_dalcoquio@yahoo.com.br')
            I.fillField('password', secret('123456'))

            I.click('.btn-default')

          },
          // if we see `Admin` on page, we assume we are logged in
          check: (I) => {
             I.amOnPage('https://automationexercise.com/');
             I.see('kedo');
          }
        }
      }
    },
    stepByStepReport: {
      enabled: true, //FALSE para desativar o relatório
      deleteSuccessful: false,
      fullPageScreenshots: true,
      screenshotsForAllureReport: true
    },
    mocha: {
      reporterOptions: {
          //reportDir: output
      }
    },
    allure: {
      enabled: true
    },
  },
  include: {
    "I": "./steps_file.js",
    "home_page": "./pages/home_page.js",
    "cadastro_page": "./pages/cadastro_page.js"
  },
bootstrap: async () => {
  await server.start();
},
teardown: async () => {
  await server.stop();
},
  name: 'projeto03r'
}